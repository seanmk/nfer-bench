#!/bin/bash
#SBATCH --mail-user=seank@cs.aau.dk
#SBATCH --mail-type=FAIL
#SBATCH --partition=rome
#SBATCH --mem=10G   # might need more to run gcc...

NFER="${1}"
HOWMANY="${2}"
SPEC="${3}"
EVENTS="${4}"
TIME="${5}"
BENCHDIR="${6}"
WORKDIR="${7}/${SLURM_JOBID}"
REPL="${8}"

echo "[Mon]nfer=${NFER},howmany=${HOWMANY},spec=${SPEC},events=${EVENTS},time=${TIME},benchdir=${BENCHDIR},repl=${REPL}"

NFER_COMMIT=`git -C "${NFER}" log --pretty=format:'%h' -n 1`

# build the monitor on the NFS mount so it's accessible to all the nodes
TMP="${HOME}/tmp"
# create the tmp dir if it doesn't exist
mkdir -p "${TMP}"

# set up -- compile the spec
SPEC_BASE=`echo ${SPEC} | sed "s|^\([^\.]*\)\.nfer|\1|"`
TMP_BASE="${TMP}/${SPEC_BASE}-${SLURM_JOBID}"
TMP_SPEC="${TMP_BASE}.nfer"
TMP_C="${TMP_BASE}.c"
TMP_C_HM="${TMP_BASE}_${HOWMANY}.c"
TMP_L2_ERR="${TMP_BASE}_${HOWMANY}.l2err"
TMP_MONITOR="${TMP_BASE}_${HOWMANY}_monitor"

#echo $TMP_SPEC
#echo $TMP_C
# copy the spec to the tmp directory, using the job id
cp "${BENCHDIR}/${SPEC}" "${TMP_SPEC}"

# generate the source
"${NFER}/bin/nfer" -c "${TMP_SPEC}"
# rename to specific to how many events
mv "${TMP_C}" "${TMP_C_HM}"
# now compute the configuration
echo "Computing monitor configuration for ${TMP_SPEC}"
"${NFER}/bin/nfer" -l2 "${TMP_SPEC}" < "${BENCHDIR}/${EVENTS}" >/dev/null 2>"${TMP_L2_ERR}"
# get just the end part we want
DEFINES=$(tail -n3 "${TMP_L2_ERR}")
# delete the defines from the C file
sed -e '/#define RULE_CACHE_SIZES/d' -e '/#define NEW_INTERVALS_SIZE/d' -e '/#define VALUE_DICTIONARY_SIZE/d' "${TMP_C_HM}" > "${TMP_C_HM}.stripped"
# add the defines to the top
echo -e "${DEFINES}\n" > "${TMP_C_HM}"
cat "${TMP_C_HM}.stripped" >> "${TMP_C_HM}"
# compile it
echo "Compiling monitor ${TMP_MONITOR}"
gcc -O3 -o "${TMP_MONITOR}" "${TMP_C_HM}"
# now we can actually run the tests


CMD="${TMP_MONITOR} < ${WORKDIR}/${EVENTS}"
PROG="monitor"

sbatch --array="1-${REPL}" ./single-bench.sh "${PROG}" "${SPEC}|${HOWMANY}" "${CMD}" "${TIME}" "${NFER_COMMIT}" "${WORKDIR}" "${BENCHDIR}/${EVENTS}"
