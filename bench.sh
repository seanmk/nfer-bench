#!/bin/bash
#SBATCH --mail-user=seank@cs.aau.dk
#SBATCH --mail-type=FAIL
#SBATCH --partition=rome
#SBATCH --mem=2G

# Memory limit for user program (allows graceful handling of out-of-memory errors in your program.)
#let "m=1024*1024*10"
#ulimit -v $m

# allocate 4 cores to avoid conflicts
# run on partition "rome" to get the latest software
# turing has GPUs

NFER="${HOME}/nfer"
BENCHDIR="${HOME}/nfer-bench"
# SPEC="test_ops.nfer"
SPEC="lanl.nfer"

NFER_SCALA="${HOME}/nfer-scala"
NFER_SCALA_JAR="${NFER_SCALA}/nfer-assembly-1.1.jar"

TESSLA="${HOME}/tessla"
TESSLA_JAR="${TESSLA}/tessla-assembly-1.2.2.jar"
TESSLA_MONITOR="${TESSLA}/test_ops.jar"


WORKDIR="/scratch/$(whoami)"

START_TIME=$(date +%s)

# version for the tools without git repos accessible
TESSLA_VERSION="1.2.2"
NFER_SCALA_VERSION="1.1"

# what tests to run
MANY="10k 20k 30k 40k 50k 60k 70k 80k 90k 100k"

# replications (array configuration, 1-X)
REPLICATIONS="5"

for HOWMANY in $MANY; do
  # EVENTS="test_${HOWMANY}_ops.events"
  EVENTS="lanl_${HOWMANY}.events"

  sbatch ./bench-shell.sh "${NFER}" "${HOWMANY}" "${SPEC}" "${EVENTS}" "${START_TIME}" "${BENCHDIR}" "${WORKDIR}" "${REPLICATIONS}"
  sbatch ./bench-monitor.sh "${NFER}" "${HOWMANY}" "${SPEC}" "${EVENTS}" "${START_TIME}" "${BENCHDIR}" "${WORKDIR}" "${REPLICATIONS}"
  sbatch ./bench-scala.sh "${NFER_SCALA_JAR}" "${HOWMANY}" "${SPEC}" "${EVENTS}" "${START_TIME}" "${BENCHDIR}" "${WORKDIR}" "${REPLICATIONS}" "${NFER_SCALA_VERSION}"

  # sbatch ./bench-tessla.sh "${TESSLA_MONITOR}" "${HOWMANY}" "${EVENTS}" "${START_TIME}" "${BENCHDIR}" "${WORKDIR}" "${REPLICATIONS}" "${TESSLA_VERSION}"
done
