#!/bin/bash
#SBATCH --mail-user=seank@cs.aau.dk
#SBATCH --mail-type=FAIL
#SBATCH --partition=rome
#SBATCH --mem=2G

NFER="${1}"
HOWMANY="${2}"
SPEC="${3}"
EVENTS="${4}"
TIME="${5}"
BENCHDIR="${6}"
WORKDIR="${7}/${SLURM_JOBID}"
REPL="${8}"
VERSION="${9}"

echo "[Sca]nfer=${NFER},howmany=${HOWMANY},spec=${SPEC},events=${EVENTS},time=${TIME},repl=${REPL},version=${VERSION}"

# get java
JAVA=$(which java)

# spec, events, minimality, output
CMD="${JAVA} -Xss900m -jar ${NFER} ${BENCHDIR}/${SPEC} ${WORKDIR}/${EVENTS} true true"
PROG="nfer-scala"

sbatch --array="1-${REPL}" ./single-bench.sh "${PROG}" "${SPEC}|${HOWMANY}" "${CMD}" "${TIME}" "${VERSION}" "${WORKDIR}" "${BENCHDIR}/${EVENTS}"
