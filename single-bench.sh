#!/bin/bash
#SBATCH --mail-user=seank@cs.aau.dk
#SBATCH --mail-type=FAIL
#SBATCH --partition=rome
#SBATCH --mem=1G   # 1 GB of RAM for now

# Memory limit for user program (allows graceful handling of out-of-memory errors in your program.)
# this screws up java....
#let "m=1024*1024*1"
#ulimit -v $m

PROG="${1}"
TESTNAME="${2}"
CMD="${3}"
TIME="${4}"
VERSION="${5}"
WORKDIR="${6}"
TOCOPY="${@:7}"

echo "[SB]prog=${PROG},testname=${TESTNAME},cmd=${CMD},time=${TIME},version=${VERSION},workdir=${WORKDIR},tocopy=${TOCOPY}"

# setup working directory - running over NFS slows things down
LOGDIR="${WORKDIR}/results"
mkdir -p "${LOGDIR}"

RESULTSDIR="${HOME}/results"
mkdir -p "${RESULTSDIR}"

for f in $TOCOPY; do
  # skip if the file already exists
  if [ ! -f "${WORKDIR}/${f}" ]; then
    cp ${f} ${WORKDIR}
  fi
done

LOG="${LOGDIR}/bench-${TIME}-${SLURM_JOBID}.result"
echo "${CMD}"
/usr/bin/time -f "${PROG}|${VERSION}|${TESTNAME}|%U|%M" sh -c "${CMD}" >/dev/null 2>> "${LOG}"

cat "${LOG}" >> "${RESULTSDIR}/${PROG}-${VERSION}.result"
# cleanup
rm -f "${LOG}"