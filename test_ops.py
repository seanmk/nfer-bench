#!/usr/bin/env python3

import _nfer
import sys
import csv

spec = sys.argv[1]
test = sys.argv[2]

_nfer.load(spec)

results = []
with open(test) as eventfile:
    reader = csv.reader(eventfile, delimiter='|')
    for row in reader:
        data = {}
        if len(row) == 4:
            values = row[3].split(';')
            if len(values) > 1:
                values[1] = bool(values[1])
            data = dict(zip(row[2].split(';'),values))
        intervals = _nfer.add(row[0], int(row[1]), int(row[1]), data)
        if intervals is not None:
            results.extend(intervals)

print(len(results))