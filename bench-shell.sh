#!/bin/bash
#SBATCH --mail-user=seank@cs.aau.dk
#SBATCH --mail-type=FAIL
#SBATCH --partition=rome
#SBATCH --mem=2G

NFER="${1}"
HOWMANY="${2}"
SPEC="${3}"
EVENTS="${4}"
TIME="${5}"
BENCHDIR="${6}"
WORKDIR="${7}/${SLURM_JOBID}"
REPL="${8}"

echo "[Sh]nfer=${NFER},howmany=${HOWMANY},spec=${SPEC},events=${EVENTS},time=${TIME},benchdir=${BENCHDIR},repl=${REPL}"

CMD="${NFER}/bin/nfer ${BENCHDIR}/${SPEC} < ${WORKDIR}/${EVENTS}"
PROG="shell"

NFER_COMMIT=`git -C "${NFER}" log --pretty=format:'%h' -n 1`

#TEST="${NFER}/bench/test_ops_${HOWMANY} < ${EVENTS}"
#PROG="monitor"

#TEST="${NFER}/bench/test_ops.py ${SPEC} ${EVENTS}"
#PROG="py"

#cd ${NFER}
#TEST="Rscript test.R"
#PROG="r"

sbatch --array="1-${REPL}" ./single-bench.sh "${PROG}" "${SPEC}|${HOWMANY}" "${CMD}" "${TIME}" "${NFER_COMMIT}" "${WORKDIR}" "${BENCHDIR}/${EVENTS}"