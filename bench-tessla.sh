#!/bin/bash
#SBATCH --mail-user=seank@cs.aau.dk
#SBATCH --mail-type=FAIL
#SBATCH --partition=rome
#SBATCH --mem=2G

TESSLA="${1}"
HOWMANY="${2}"
EVENTS="${3}"
TIME="${4}"
BENCHDIR="${5}"
WORKDIR="${6}/${SLURM_JOBID}"
REPL="${7}"
VERSION="${8}"

echo "[Tsl]tessla=${TESSLA},howmany=${HOWMANY},events=${EVENTS},time=${TIME},benchdir=${BENCHDIR},repl=${REPL},version=${VERSION}"

TMP="${HOME}/tmp"
# create the tmp dir if it doesn't exist
mkdir -p "${TMP}"

CONVERTER="./nfer2tessla.py"

# this is just for logging
TESSLA_BASE=`echo ${TESSLA} | sed "s|.*\/||"`

# set up convert the events into tessla's format
EVENT_BASE=`echo ${EVENTS} | sed "s|^\([^\.]*\)\.events|\1|"`
EVENT_INPUT="${EVENT_BASE}-${SLURM_JOBID}.input"
TMP_INPUT="${TMP}/${EVENT_INPUT}"

# run the converter script
echo "Converting ${BENCHDIR}/${EVENTS} to TeSSLa input"
"${CONVERTER}" "${BENCHDIR}/${EVENTS}" > "${TMP_INPUT}"

# now we can actually run the tests
JAVA=$(which java)

CMD="${JAVA} -Xss900m -jar ${TESSLA} < ${WORKDIR}/${EVENT_INPUT}"
PROG="tessla"

sbatch --array="1-${REPL}" ./single-bench.sh "${PROG}" "${TESSLA_BASE}|${HOWMANY}" "${CMD}" "${TIME}" "${VERSION}" "${WORKDIR}" "${TMP_INPUT}"
