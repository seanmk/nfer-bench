#!/usr/bin/env python3

import sys
import re


# this script is to change the scala output to the same format as the C nfer for easier comparison

if __name__ == '__main__':
    if len(sys.argv) > 1:
        output = str(sys.argv[1])

    line_re = re.compile("  Interval\(\"([^\"]+)\",([0-9]*)\.0+,([0-9]*)\.0+,Map\(([^\)]*)\)")
    map_re = re.compile("\"([^\"]*)\" -> \"([^\"]*)\"")

    with open(output, 'r') as reader:
        for line in reader:
            result = line_re.match(line)
            if result is not None:
                name = result.group(1)
                start = result.group(2)
                end = result.group(3)
                map_str = result.group(4)

                if len(map_str) > 0:
                    map_result = map_re.match(map_str)
                    map_key = map_result.group(1)
                    map_val = map_result.group(2)

                    print('%s|%s|%s|%s|%s' % (name, start, end, map_key, map_val))
                else:
                    print('%s|%s|%s' % (name, start, end))

